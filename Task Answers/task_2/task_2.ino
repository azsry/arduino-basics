int i = 0;

void setup() {
  // put your setup code here, to run once:
  // set up a connection with the Serial Monitor
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  // we only want numbers from 0 - 100
  if( i > 100 )
    return;

  // we need our special message to take priority, otherwise 77 will fit in the odd category before reaching the special check
  if ( i == 77 ) {
    Serial.println( String( i ) + " is a cool number!" );
  }

  /* % gives the remainder after doing a division, so numbers with a remainder after being divided by 2 are odd 
     this also has to be in an else if so that 77 does not match both the 'cool' and 'odd' conditions */
  
  else if( i % 2 ) {
    Serial.println( String( i ) + " is an odd number" );
  }

  // inversely, numbers that do not have a remainder after being divided by 2 are even
  else if( !( i % 2 ) ) {
    Serial.println( String( i ) + " is an even number" );
  }
  
  // increment our number
  i++;
  delay(20);
}
