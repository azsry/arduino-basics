float i = 0;

void setup() {
  // put your setup code here, to run once:
  // Initialise a link to the Serial Monitor
  Serial.begin( 9600 );
}

void loop() {
  // put your main code here, to run repeatedly:
  // Print the current value held in 'i' to the Serial Monitor
  Serial.println( String( i ) + "\r\n" );

  // Increase i's value by 0.5
  i += 0.5;
}
