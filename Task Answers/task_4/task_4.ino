int ledPins[3] = {2, 3, 4};
int y = 0;
const int num_leds = sizeof(ledPins) / sizeof(int);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // sizeof(ledPins) will return how many bytes it takes to store the array. Since the array is of data-type int (a 2-byte value), the value will be equal to 6 by itself.
  // To get the number of elements inside the array, we simply divide it by sizeof(int) to figure out how many times an int is stored in the array.
  // This will always work because arrays hold more than one data-type
  for ( int i = 0; i < num_leds; i++ ) {
    pinMode(ledPins[i], OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:

  // Disable all lights
  for ( int i = 0; i < num_leds; i++ ) {
    if (digitalRead(ledPins[i]) == HIGH)
      digitalWrite(ledPins[i], LOW);
  }

  delay(10);

  /*  This will cycle between 0, 1 and 2 based on i's value.
      Remember that the modulo operator returns the remainder after the closest integer division. 
      This means that the value will always be between 0 and num_leds - 1, which is perfect when 
      accessing arrays via index. */
  digitalWrite(ledPins[y % num_leds], HIGH);
  y++;
  delay(1000);
}
