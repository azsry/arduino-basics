void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  /* 
    makes a for-loop that
      - initialises i to 0
      - runs until i == 100
      - increases the value of i each iteration 
  */
  for(int i = 0; i < 100; i++) {
    if( i == 42 )
      Serial.println( "Yay! 42" );
    else
      Serial.println(String(i));
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
