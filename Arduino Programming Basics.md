# Arduino Programming Basics <!-- omit in toc -->

## Written by Alwyn Wan <!-- omit in toc -->

- [Basic Syntax](#Basic-Syntax)
- [Basic Terminology](#Basic-Terminology)
  - [Comments](#Comments)
  - [Variables](#Variables)
  - [Operators](#Operators)
    - [Assignment vs Comparison](#Assignment-vs-Comparison)
    - [Task #1](#Task-1)
  - [Functions](#Functions)
    - [Calling functions](#Calling-functions)
  - [If/Else/Else-If Statements](#IfElseElse-If-Statements)
    - [Task #2](#Task-2)
  - [For-/While-loops](#For-While-loops)
    - [For-loop](#For-loop)
    - [While Loop](#While-Loop)
    - [Break and Continue](#Break-and-Continue)
    - [Task #3](#Task-3)
- [Advanced Terminology](#Advanced-Terminology)
  - [Includes](#Includes)
  - [Arrays](#Arrays)
    - [Task 4](#Task-4)
  - [Classes](#Classes)
  - [Structs](#Structs)
  - [Enumerations](#Enumerations)
- [Conclusion](#Conclusion)

## Basic Syntax

The Arudino programming language is heavily based off of the C programming language. The basic syntax of a line of code in C-style languages is:

```cpp
printf("Hello, world! \n");
```

Semicolons are used to denote the end of a statement. Brackets are used to encapsulate certain parts of code that may need to be interpreted before others, or to denote argument lists for functions. Function and variable names are all case sensitive.

<div style="page-break-after: always;"></div>

## Basic Terminology

Let’s get started with some basic terminology. We’ll start with a small code snippet that contains many elements, and work through each part.

```cpp
// Fibonacci sequence generator
int max_num = 100;
int x = 0;
int y = 1;

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  Serial.println( String( x ) + "\n" + String( y ) );
  for( int z = 0; ; ) {
    z = x + y;
    if ( z > max_num )
        break;
  
    Serial.println( String( z ) );
    x = y;
    y = z;
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
```

As the first line suggests, this piece of code will generate fibonacci numbers between 0 and whatever max number you provide. Don't worry if the snippet looks scary right now. We'll step through each part.

<div style="page-break-after: always;"></div>

### Comments

`// Fibonacci sequence generator`

This is a ‘comment’. Comments are often used to explain what certain parts of code do, or to notify other collaborators of places that are yet to be finished. Single line comments are preceded by two forward-slashes, like in the snippet. If you wish to write an essay in a comment, you can encase the chunk of text you want commented with `/* */`:

```cpp
/* 	This is a comment that
	spans over multiple lines. It can be formatted 	
		however	
			you
				wish,
	and it will keep its formatting 
	because    it 
	uses       a 
	monospaced font. */
```

### Variables

`int max_num = 100;`

This is how you define a variable. Variables are used to hold pieces of data you wish to access or alter during program execution. The general formatting of defining a variable is as follows:

```cpp
<data type> <variable name> = <some data>;
```

`<data type>` defines what type of data the variable will hold. Here is a small, non-exhaustive list of data types available in almost every programming language. Different languages may give data types different labels, but these are the ones that the Arduino language (and many other C-based langages) use:

- `char`: a char(acter) holds a single byte of data representing a character that can (most often) be displayed on the screen. Chars are usually defined with single quotation marks.
  - e.g. `char a = 'a';`
- `string`: a string is a 'string' of chars that can form words. They are usually defined using double quotation marks.
  - e.g. `string demo = "Hello, world!";`
- `int`: an int(eger) holds whole numbers which do not have numbers trailing after a decimal point. On the Arduino Uno (and other ATmega-based boards), int's are stored as 2-byte values, meaning they can hold a maximum value of 65535.
  - e.g. `int x = 21514;`
- `float`: a float represents numbers that have trailing decimal points.
  - e.g. `float x = 4676.1104;`
- `double`: doubles are similar to floats, but have the added benefit of double-precision when managing numbers after the decimal point. In practice, they are used the same as `floats`.
  - e.g. `double x = 4676.1104;`
- `bool`: bool(ean)s are used to hold a true/false value, and are often used as passing conditions for `if` statements.
  - e.g. `bool needs_watering = true;`
- `long`: long is prepended to number data-types such as `char`, `int`, `float`, and `double` to extend the maximum numerical values able to be stored in that variable. Using `long` by itself as a data-type will default to `long int`.
  - e.g. `long = 123054302495365;`
- `signed/unsigned`: unsigned is also prepended to a numerical data-type to alter the storage range of a variable. Signed variables can be either positive or negative, while unsigned variables can only be positive. Variables are signed by default. The potential range the variable can hold is the same regardless, but because signed variables can go both positive and negative, the maximum value able to be described by a signed variable is only half that of its unsigned counterpart.
  - e.g.
  
    ```cpp
    Serial.begin( 9600 );
    int max_int = 32767; // This is equal to 2^15 - 1
    int min_int = -32768;
    unsigned int max_uint = 65535; // 2^16 - 1
    unsigned int min_uint = 0;

    Serial.println( "Max. int value: " + String(max_int) + "\n" + 
                    "Min. int value: " + String(min_int) + "\n" + 
                    "Max. int value + 1: " + String(max_int + 1) + "\n" +
                    "Max. uint value: " + String(max_uint) + "\n" + 
                    "Min. uint value: " + String(min_uint) + "\n" +
                    "Max. uint value + 1: " + String(max_uint + 1));

    Output:
    Max. int value: 32767
    Min. int value: -32768
    Max. int value + 1: -32768
    Max. uint value: 65535
    Min. uint value: 0
    Max. uint value + 1: 0
    ```

    - As you can see, once the maximum has been exceeded, it will start counting back up from the minimum value. The reverse is true if you go lower than the minimum value.
- `void`: void is a none data-type, and cannot hold data. Therefore, it cannot be used when defining variables. Instead, it is used when defining a function that does not need to return any data.

<div style="page-break-after: always;"></div>

`<variable name>` can be whatever you want to call it, but it should be self-explanatory to third parties reading your code, or at the very least, yourself. There are some guidelines, however:

- Variable names cannot start with a digit
- Variable names must consist of alphanumeric characters, and _ only.
- There are different methods of formatting variable names; it is really up to personal preference which you use. Here are some of the popular choices:
  - Pascal Case: Words are concatenated together, and the first letter of each word is capitalised:
    - e.g. `string SomeString = ...;`
  - Camel Case: Similar to Pascal Case, but the first letter of the first word is not capitalised:
    - e.g. `string someString = ...;`
  - Snake Case: All letters are lowercased, and all spaces are replaced with underscores:
    - e.g. `string some_string = ...;`
- It does not matter which you end up using, but once you have chosen one, be consistent.

`<some data>`'s composition depends on the data type you decide to use. Notice how each variable created in the `<data type>` section had slightly different formatting for the data.

### Operators

There are several basic operators you can use to manipulate data. These are:

```
+ : Arithmatic addition, string concatenation
- : Arithmatic subtraction
* : Arithmatic multiplication
/ : Arithmatic division
% : Modulo (returns the 'remainder' after doing the closest possible integer division)
++: Increment value by 1
--: Decrement value by 1
+=: Increment value by a specified amount (x += y equivalent to x = x + y;)
-=: Decrement value by a specified amount (x -= y equivalent to x = x - y;)
*=: Multiply a value by a specified amount (x *= y equivalent to x = x * y;)
/=: Divide a value by a specified amount (x /= y equivalent to x = x / y;)
%=: Assign the modulo value to a variable (x %= y equivalent to x = x % y;)
```

<div style="page-break-after: always;"></div>

#### Assignment vs Comparison

One important, and often overlooked distinction is how the `=` symbol works in C-style languages. One `=` refers to an 'assignment', for example `x = 10;` will assign `x` a value of `10`. Two `=`'s is referred to as a comparison. This is used for boolean operations where you want to know whether a certain condition is true. For example: `y = x == 10;` will assign `y` a true/false value that corresponds to whether `x` is equal to `10`. There are also some comparison operators, some of which require two characters:

```
== : Comparative equality
!= : Comparative inequality
|| : Logical OR
&& : Logical AND
>  : Greater than
<  : Less than
>= : Greater than or equal to
<= : Less than or equal to
```

Additonally, there are operators used specifically for bit-related operations. Do not worry about these for now. They are just listed here for completeness:

```
&    : bitwise AND
|    : bitwise OR
~    : bitwise NOT
^    : bitwise XOR
<<   : bitwise left shift
>>   : bitwise right shift
>>>  : bitwise unsigned right shift
&=   : bitwise AND assignment
|=   : bitwise OR assignment
^=   : bitwise XOR assignment
<<=  : bitwise left shift and assignment
>>=  : bitwise right shift and assignment
>>>= : bitwise unsigned right shift and assignment
```

#### Task #1

```
Write a program that prints numbers increasing in 0.5 increments (i.e. output should be 0.0, 0.5, 1.0, 1.5...).
```

<div style="page-break-after: always;"></div>

### Functions

```cpp
void setup( ) {
    ...
}
```

Functions are small blocks of code that can be easily called at other places in the code without having to repeat the same lines multiple times. This is especially useful where you know you will be doing the same task continuously, or require some special feature that would look nicer as a one-liner than spread over multiple, duplicated lines.

Function definition syntax looks as follows:

```
<return type> <function name>(<arguments>) {
    <code inside the function>
}
```

`<return type>` defines the data type that the function will return once it has finished execution. `void` can be used here if the function does not need to return any data. Functions that are not `void` return type MUST return a value of that correct type. A common example of this would be:

```cpp
int main( ) {
    // some code here
    return 0;
}
```

0 is often used to indicate a successful execution, but anything can be returned. Different numbers are often returned so errors can be diagnosed:

```cpp
int main( ) {
    // some code here
    if( some_condition )
        return -1;

    // some more code

    if( some_other_condition )
        return -2;

    // even more code
    return 0;
}
```

If a return line is executed, code below it will not be run:

```cpp
int test = 2;

int test_func() {
  if ( test == 3 ){
    Serial.println("test was equal to 3");
    return 3;
  }

  else if( test == 2 ) {
    Serial.println("test was equal to 2");
    return 2;
  }

  else if( test == 1 ) {
    Serial.println("test was equal to 1");
    return 1;
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );

  Serial.println( test_func( ) );
}

Output:
test was equal to 2
2
```

Because functions return a certain data type, they can be used just as any other data of that type, such as as an argument for another function, or as part of a variable's value assignment:

```cpp
int add( int a, int b ) {
    return a + b;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  int ans = add( 2, 3 ); // The int value returned from 'add' assigned to 'ans'
  Serial.println( add( 2, 3 ) ); // The int value returned from 'add' is used as an argument for println
}
```

<div style="page-break-after: always;"></div>

`<function name>` defines the name of the function and will be used for subsequent calls of the function. Function names must follow the same conditions as listed above for variable names.

`<arguments>` are a list of variables, separated with a comma (,), which can be accessed inside the function. This allows you to send data to the function from outside the function, which the function can use or modify internally before returning its final value. For example:

```cpp
Serial.println( String( a ) ); // a is not defined

int add( int a, int b ) {
    return a + b;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  int ans = add( 2, 3 );
  Serial.println( ans );
}

Output:
5
```

So why use functions? For example, the following code would work just fine, but does not look very elegant:

```cpp
int x = -24;
if ( x > 100)
    x = 100;
if (x < 0)
    x = 0;
```

<div style="page-break-after: always;"></div>

This code would ensure x's value is always between 0 and 100. If you needed to do this many times, but with different min/max values, your code would start to get quite unwieldy. One option would be to encase most of the repeatable code in a function:

``` cpp 
int clamp( int value, int min, int max ) {
    if( value > max )
        value = max;
    if ( value < min )
        value = min;
    return value;
}
```

You can now call the `clamp` function and the same code as above would be run, but now it can apply to multiple scenarios with just one line, and will work with any min/max value:

```cpp
x = -24;
x = clamp( x, 0, 100 );
```

Similarly to naming variables, functions should be named accurately according to what they do. They should be self-explanatory to third-parties reading your code, or at the very least, to you.

- e.g `int doTheThing( int x ) { }` vs. `int next_prime( int current_prime ) { }`

Function arguments can also be optional. These arguments are defined with a default value in the argument list, but that default value will be overwritten if the user specifies a value for that argument while calling the function:

```cpp
int generate_number( bool even = true ) {
    int num = 0;
    do {
        num = rand( ) % 1000;
    } while( ( !even && num % 2 == 0 ) || ( even && num % 2 != 0 ) );
    return num;
}
```

If the user simply called `int x = generate_number( );`, x would be assigned an even number between 0 and 1000. However, if the user called `int x = generate_number( false );`, x would be assigned an odd number.

<div style="page-break-after: always;"></div>

#### Calling functions

`Serial.begin(9600);`

`Serial.println( String(x) + "\n" + String(y) );`

Functions can be called by simply typing their name, then providing arguments as required by the specific function. In our example above with the `clamp` function, we defined the function as `clamp`, so to call it, we would simply type `clamp(x, 0, 100);`, where `x`, `0` and `100` are arguments which correspond with `value`, `min`, and `max` in the function's definition.

In `Serial.begin(9600);`, the function name is `begin`, and `Serial` refers to a struct with member functions. Don't worry about this too much for now, but you can think of the struct as a way to organise information.

`"\n"` in the code snippet represents a newline character for the computer. These are important for formatting strings, but some functions may already add one at the end without you needing to. For example, `Serial.println` means "print line". If you use multiple `println`'s, they will appear on separate lines by default, so you do not need a `"\n"` at the very end. Using a backslash (\\) in conjunction with certain characters is called 'escaping' them. Some characters are also integral parts of the C language, so to display them, you will also need to escape them. Some special escape sequences are:

```
\': Single quote
\": Double quote
\\: Backslash
\t: Horizontal tab
\v: Vertical tab
\u: Unicode specifier
\n: New line
\r: Carriage return (used in conjunction with \n on Windows, but not Mac or Linux)
\0: Null terminator (used to signify the end of a null-terminated string)

```

<div style="page-break-after: always;"></div>

### If/Else/Else-If Statements

```cpp
if ( z > max_num )
    break;
```

If statements are very common in programming, and allow you to run certain parts of code only if some conditions are met. The basic syntax is shown below:

```cpp
if ( <condition> ) {
    // code run if condition is true
}
```

`<condition>` must always contain statements that a `bool` value, whether explicitly or implicitly. Ideally, your statements should include a comparative operator (i.e. `==`, `!=`, `>`, `<`, etc.), or a function that returns a `bool` value.

You can nest multiple `if` statements within each other, or group them together in the same `if` statement:

```cpp
if ( condition1 ) { 
    if ( condition2 ) {
        // code to be run if condition1 AND condition2 are true
    }
    if ( condition3 && condition4 || ( condition5 && condition6 ) ) {
        // code to be run if condition3 AND condition4, OR condition5 AND condition6 are true
    }
    // code to be run if only condition1 is true
}
else if ( condition7 ) {
    // code to be run if condition1 is false, but condition7 is true
}
else {
    // code to be run if neither condition1 nor condition7 are true
}
```

It is important to remember the order in which you want the conditions to be interpreted. For example:
`if ( condition1 && condition2 || condition3 ) { }` will enter the code block if `condition1` is true, and either of `condition2` or `condition3` are true. On the other hand, `if( (condition1 && condition2) || condition3 ) { }` will enter the code block if `condition1` and `condition2` are true, or if `condition3` is true. By default, conditions are interpreted from left to right.

`else` and `else if` are used in tandem with `if` statements. `else` is used as a final catch-all to run any code necessary if all `if` statements are false, while `else if` is used to check additional conditions.

<div style="page-break-after: always;"></div>

There are some scenarios where `else if` is not required. For example:

```cpp
if ( z > max )
    z = max;
if ( z < min )
    z = min;
```

Regardless of whether there is an else on `if ( z < min )`, the code will run the same. Whether you use an `else if` in this scenario is totally up to personal preference, but if in doubt, use it.

#### Task #2

```
Write a program that prints out different messages depending on whether the an incremented number is even or odd, and print a special message if the number is equal to 77, for numbers between 0 and 100.
```

### For-/While-loops

#### For-loop

```cpp
for( int z = 0; ; ) {
    ...
}
```

A for-loop is a special piece of syntax that tells the program to repeat a block of code until some condition is met. The syntax for a for-loop is as follows:
`for( <initialisation>; <condition>; <increment>) {}`

`<initialisation>` allows you to enter a statement that will be run once, and only once, before the loop begins. This is most often used to initialise a counting variable.

`<condition>` defines the condition that must be met for the code inside the loop to run. This is checked after each iteration of the loop is completed, and the loop will not run on the iteration that it is met. For example, if the condition is `i < 100`, the loop will not be run if `i == 100`.

`<increment>` allows you to enter a statement that will be run after each iteration's completion, and before the next iteration. This is often used to increment or decrement counting variables.

You may have noticed that the code in the snippet does not include a condition or increment statement. (In fact, all three statements are optional, but having no exit condition will make the for-loop run indefinitely.) This is because there is no need for an increment statement for this specific for-loop, and the exit condition is best checked during an iteration, rather than after one has completed.

<div style="page-break-after: always;"></div>

#### While Loop

```cpp
void setup( ) {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  int x = 0;
  while( x < 100 ) {
    Serial.println( String( x ) );
    x++;
  }
}

Output:
1
2
3
...
99
```

While loops provide near-identical functionality to a for-loop, but they lack the ability to specify `initialisation` and `increment` statements. This means a counting variable must be created before the while-loop block is defined, and any incrementing must be done at the bottom of the loop, such as the `x++` at the bottom of the snippet above. Because of this, for-loops are often favoured where counting variables are involved, and while-loops are often reserved for situations where you wish to temporarily halt the execution of a program to ensure some condition is met. For example, you may do something like this during program initialisation:

```cpp
int temp_sensor_state = get_sensor_state( SENSOR_TEMP );
while( temp_sensor_state != SENSOR_STATE_READY ) {
    temp_sensor_state = get_sensor_state( SENSOR_TEMP );
}
```

As you may have realised, this can be inefficient as you have to repeat a line of code almost exactly. You can use a `do-while` loop to combat this:

```cpp
int temp_sensor_state;
do {
    temp_sensor_state = get_sensor_state( SENSOR_TEMP );
} while( temp_sensor_state != SENSOR_STATE_READY );
```

`temp_sensor_state` has to be defined earlier in the program for either of these to work, but the `do-while` version allows you to defer assigning a meaningful value to `temp_sensor_state`, thus removing the need to repeat code.

#### Break and Continue

```cpp
if( z > max_num )
    break;
```

```cpp
if( sensor_state != SENSOR_STATE_READY )
    continue;
```

`break` and `continue` are special keywords that can only be used inside a for- or while-loop. `continue`, rather contrary to its natural meaning, does not continue the flow of the code outside the block. This is what `break` does. `break` will halt execution of a for-/while-loop immediately and continue with the code following the end of the loop block. `continue` skips the remaining code in the current iteration and begins the next iteration.

e.g.

```cpp
for( int i = 0; i < 100; i++ ) {
    if ( i == 50 )
        break;
    Serial.print( String( i ) + ", " );
}
Serial.println( "\nHello!" );

Output:
0, 1, 2, 3..., 49,
Hello!
```

```cpp
for( int i = 0; i < 100; i++ ) {
    if ( i == 50 )
        continue;
    Serial.print( String( i ) + ", " );
}
Serial.println( "\n Hello!" );

Output:
0, 1, 2, 3..., 49, 51, 52..., 99,
Hello!
```

Now we have gone through each bit of terminology used in the very initial code snippet. Hopefully when you go back and read it now, it will make more sense. Don't worry if you don't understand what all the variable assignments are for. That's more specific for how a Fibonacci sequence is generated.

<div style="page-break-after: always;"></div>

#### Task #3

```
Write a program that increments a variable, prints each number to the Serial Monitor, but prints a special message upon reaching 42.
```

## Advanced Terminology

The next few sections will cover some other useful, but slightly more advanced, programming techniques.

### Includes

```cpp
#include <Servo.h>
```

Includes are used to include external headers into your program. This gives you access to a large group of standard C libraries (groups of pre-made functions), and also libraries written especially for Arduino. These will be used quite often when working with different sensors, or with external libraries downloaded from inside the Arduino IDE.

Includes can be placed anywhere in a source file, so long as it comes before any functions from it are referenced in the code. It is good practice, however, to place all includes at the top of a file.

The ordering of includes also matters. If an included file requires functionality from another header, the header that contains said functionality must be included before the one that requires the functionality. Incorrect header ordering will result in the program being unable to compile.

<div style="page-break-after: always;"></div>

### Arrays

```cpp
int brightness_levels[ 5 ] = { 0, 25, 50, 75, 100 };
for(int i = 0; i < sizeof( brightness_levels ) / sizeof( int ); i++)
    Serial.println( "Brightness Level " + String( i ) + ": " + String( brightness_levels[ i ] ) );

Output:
Brightness Level 0: 0
Brightness Level 1: 25
Brightness Level 2: 50
Brightness Level 3: 75
Brightness Level 4: 100  
```

Arrays are used to hold various pieces of related data. Array initialisation syntax is as follows:

```
<data type> <array name>[<number of elements>] = {<initialisation values>};
```

This line initialises an array with a fixed size, and you must include no more than that fixed number of elements in the `<initialisation values>` section. If you include less values, the rest will be filled with the data type's default value (e.g. `0` for `int`, `0.0f` for `float` and `double`, `""` for `char` and `string`). Arrays can also be initialised without any `<initialisation values>` specified, which will initialise every element in the array with the data type's default value.

Note that `<number of elements>` starts from 1: [ 5 ] means 5 values, but the actual elements in the array start from 0, and go up to `<number of elements> - 1`:

```cpp
int brightness_levels[ 5 ] = { 0, 25, 50, 75, 100 };
Serial.println( String( brightness_levels[ 0 ] ) + "\n" + String( brightness_levels[ 4 ] ) );

Output:
0
100
```

<div style="page-break-after: always;"></div>

#### Task 4

```
Create a circuit with 3 LED's (ideally with different colours) and connect them to 3 separate pins. Define these pin numbers in an array and toggle which one is lit every 100ms.
```

As there is quite a difficulty jump from Tasks 3 to 4, you may find it useful to tackle this problem in stages:

1. Make a circuit with 3 LEDs connected to three separate pins.
2. Make an array in your program that holds the pin numbers from left to right in your circuit.
3. Make a program that can light up an LED by getting the pin number from an array.
4. Change the program to light up all three LEDs.
5. Find a way to turn all three LEDs on in sequential order (HINT: you already have the pin numbers in order, in an array).
6. Update the program to turn off ONLY LEDs that are on.
7. EXTRA: Change the program to work with any number of LEDs (i.e. no hardcoded number of LEDs in for-loops).

<div style="page-break-after: always;"></div>

### Classes

```cpp
class Sensor {
public:
    // Default constructor
    Sensor() {
        pinNum = 0;
        sensitivity_threshold = 0;
        active = false;
        safe_name = "";
    }

    Sensor( int _pinNum, int _sens, bool _active = false, String _safe_name = "" ) {
        pinNum = _pinNum;
        sensitivity_threshold = _sens;
        active = _active;
        safe_name = _safe_name;
    }

    // Default destructor
    ~Sensor() {

    }

    int get_pin_num( ) {
        return pinNum;
    }

    int set_pin_num( int pin ) {
        pinNum = pin;
    }

    String get_name( ) {
      return safe_name;
    }

    String set_name( String new_name ) {
      safe_name = new_name;
    }

private:
    // Member variables
    int pinNum;
    int sensitivity_threshold;
    bool active;
    String safe_name;
protected:
};
```

<div style="page-break-after: always;"></div>

```cpp
void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  Sensor temp_sensor = Sensor(3, 100, false, "Dome Temperature");
  Serial.println( temp_sensor.get_name( ) + " pin: " + temp_sensor.get_pin_num( ) );
}

Output:
Dome Temperature pin: 3
```

Classes are very common in programming, and are used to define an 'object' and its properties. Classes have three levels of member variable access: `public`, `protected`, and `private`:

- `Public` members of a class are accessible by anything. Public is normally used for functions that you want to be exposed to the rest of your code, and also for constructor and deconstructors.
  - **Constructors** (signified as a function with simply the class name, and no data type) are special member functions run when the class is first created. This is useful for setting up default data for the class.
  - **Deconstructors** are the opposite of constructors: they are run when the class object is deleted, and are often used to stop any member functions which may still be running, and perform memory cleanup before the class object is deleted.
- `Protected` members of a class are not accessible outside of the class's code, but are accessible from the code of any class derived from this class. Protected member variables are rarely used.
- `Private` members of a class are not accessible outside of the class' code, nor from the code of any class derived from this class. Private is most often used to define member variables which are unique to each class object. It is good practice to keep member variables in `private`, and only allow reading/writing of member variables through explicitly created `public` functions. This will allow for only certain member variables to be exposed at any one time.

Classes may have zero or more public, private or protected variables, but all classes must have a default constructor and deconstructor without any arguments so that they can be used with arrays.

A class object's **public** members (and **protected** members for derived classes) are accessed using a period (.):

```cpp
Sensor temp_sensor = Sensor( 0, 100, false, "Dome Temperature" );
temp_sensor.set_pin_num( 6 );
Serial.println( String( temp_sensor.get_pin_num( ) ) );

Output:
6
```

<div style="page-break-after: always;"></div>

### Structs

```cpp 
struct sensor_s {
    sensor_s() {
        pinNum = 0;
        sensitivity_threshold = 0;
        active = false;
        safe_name = "";
    }

    sensor_s(int _pinNum, int _sens, bool _active = false, String _safe_name = "") {
        pinNum = _pinNum;
        sensitivity_threshold = _sens;
        active = _active;
        safe_name = _safe_name;
    }
    int pinNum;
    int sensitivity_threshold;
    bool active;
    String safe_name;
};
```

Structs are similar to classes in that they hold multiple pieces of data that correspond to one object. They are useful for bundling relevant data together. Structs must have a default constructor that contains no arguments so that the struct can be used with arrays. Structs do not have the ability to control access to members. All are the equivalent of a class' 'public' members.

<div style="page-break-after: always;"></div>

Struct members are accessed using a period (.):

```cpp
struct sensor_s {
    sensor_s() {
        pinNum = 0;
        sensitivity_threshold = 0;
        active = false;
        safe_name = "";
    }

    sensor_s(int _pinNum, int _sens, bool _active = false, String _safe_name = "") {
        pinNum = _pinNum;
        sensitivity_threshold = _sens;
        active = _active;
        safe_name = _safe_name;
    }
    int pinNum;
    int sensitivity_threshold;
    bool active;
    String safe_name;
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  sensor_s temp_sensor = sensor_s( 0, 100, false, "Dome Temperature" );
  Serial.println( temp_sensor.safe_name + " threshold: " + String( temp_sensor.sensitivity_threshold ) );
}

Output:
Dome Temperature threshold: 100
```

<div style="page-break-after: always;"></div>

### Enumerations

```cpp
enum sensor_state_t {
    SENSOR_STATE_READY = 0,
    SENSOR_STATE_CALIBRATING,
    SENSOR_STATE_OFF,
    SENSOR_STATE_ERROR,
    SENSOR_STATE_MAX
} sensor_state;
```

Enumerations are used most commonly in relation with **state machines**. They are used to keep track of the status of something without resorting to using numbers. They translate back to an implementation-defined integral type. This is most commonly an `int`, but can change if the value of any enum state exceeds the maximum value able to be stored by an `int` type. Enumerations make status reporting much easier for human readers. The basic syntax for defining and enum is:

```
enum <enum name> {
    <enum state name> = <value>,
    ...
};
```

Each `<enum state name>` can be allocated a specific `<value>`, which do not need to be contigious, but ideally should be. States that are not explicitly assigned values will be implicitly assigned a value one higher than the state before it. For example:
```
enum sensors_s {
    SENSOR_READY = 9,
    SENSOR_CALIBRATING = 3,
    SENSOR_OFF, // 4
    SENSOR_ERROR = 6,
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  Serial.println( SENSOR_OFF );
}

Output:
4
```

Because enums just translate back to an integral data type, it is not possible to get the length of an enum using a function. Instead, it is good practice to include a final `MAX` enum entry, which will correspond to the size of the enum, and can be referred to later on in code. However, if `MAX` is not given a value, and the enum is not created in contigious, increasing numerical order, it will be assigned a value one higher than the previous state's value, not the number of elements in the enumerator:

```cpp
enum sensors_s {
    SENSOR_READY = 9,
    SENSOR_CALIBRATING = 3,
    SENSOR_OFF, // 4
    SENSOR_ERROR = 6,
    SENSOR_MAX // 7
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  Serial.println( SENSOR_MAX );
}

Output:
7
```

As seen above, there are only 5 elements in the enum, and `SENSOR_MAX`'s value should ideally be equal to 4 (because the first element is usually allocated the value 0):

```cpp
enum sensors_s {
    SENSOR_READY = 0,
    SENSOR_CALIBRATING,
    SENSOR_OFF,
    SENSOR_ERROR,
    SENSOR_MAX
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  Serial.println( SENSOR_MAX );
}

Output:
4
```

## Conclusion

That should about do it for learning the basics of programming in C-style languages. What you do from here is totally up to you and your creative imagination. Get some sensors, LEDs, motors etc, and start programming to add functionality to your contraptions!

As a final word of advice, you may find it useful to write out the steps your program should go through in plain English before trying to write it in programming syntax. This will help you to conceptually understand what you are doing, and helps you figure out whether you actually know what your program needs to do, and in what order. This is where comments really come in handy. If you go through the answers I have provded in the `Task Answers` folder, you will see that I have documented each line so that even a person who has no clue what the code does will be able to follow and at least have a logical understanding of the steps involved.
